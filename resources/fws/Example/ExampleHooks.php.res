<?php
declare( strict_types = 1 );

namespace FWS\Custom\Example;

use FWS\Framework\SingletonHook;

/**
 * Class ExampleHooks
 *
 * @package FWS\Custom\Example
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
class ExampleHooks extends SingletonHook
{

	/** @var self */
	protected static $instance;

	/**
	 * Just some example, doin' nothing
	 */
	public function exampleHookCallback(): void
	{
		wp_die( 'Yup, this is working.');
	}

	/**
	 * Drop your hooks here.
	 */
	protected function hooks()
	{
		// Actions
		// add_action( 'init', [ $this, 'exampleHookCallback' ] );
	}
}
