<?php
declare( strict_types = 1 );

namespace FWS\Custom\Example;

/**
 * Class Example
 *
 * @package FWS\Framework\Example
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
class Example extends \FWS\Framework\Example\Example
{

	/** @var self */
	protected static $instance;

	/**
	 * Outputs an example string
	 */
	public function exampleMethod(): void
	{
		// This will call the same method from the extended (parent) Class
		parent::exampleMethod();

		// You can remove the parent call, and implement an entirely different logic here
		echo 'Outputted from a child class.<br>';
	}
}
