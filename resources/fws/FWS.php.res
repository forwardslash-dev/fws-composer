<?php
declare( strict_types = 1 );

namespace FWS\Custom;

use FWS\Custom\Example\Example as CustomExample;
use FWS\Custom\Example\ExampleHooks;
use FWS\Framework\Example\Example as FrameworkExample;
use FWS\Framework\FWS as FWSFramework;
use FWS\Framework\ACF\Render as FrameworkACFRender;
use FWS\Framework\Config\Config as FrameworkConfig;
use FWS\Framework\Theme\Images as FrameworkThemeImages;
use FWS\Framework\Theme\Render as FrameworkThemeRender;
use FWS\Framework\Theme\Styleguide as FrameworkThemeStyleguide;
use FWS\Framework\WC\Render as FrameworkWCRender;

/**
 * Singleton Class FWS
 *
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
class FWS extends FWSFramework
{

	/** @var FWS */
	protected static $instance;

	/** @var FrameworkWCRender */
	protected $wc;

	/** @var FrameworkACFRender */
	protected $acf;

	/** @var FrameworkThemeRender */
	protected $render;

	/** @var FrameworkThemeImages */
	protected $images;

	/** @var FrameworkThemeStyleguide */
	protected $styleguide;

	/** @var FrameworkConfig */
	protected $config;

	/** @var FrameworkExample */
	protected $example;

	/**
	 * Hooks Classes initialization
	 */
	protected function hooks(): void
	{
		/**
		 * @uses \FWS\Framework\Theme\Hooks::init()
		 * @uses \FWS\Framework\ACF\Hooks::init()
		 * @uses \FWS\Framework\WC\Hooks::init()
		 */
		parent::hooks();

		// Initialize custom hook classes here
		// Example:
		ExampleHooks::init();
	}

	/**
	 * @return FrameworkExample
	 */
	public function example(): FrameworkExample
	{
		// This will simply return the result from the extended (parent) class:
		return parent::example();

		// But you can replace it with your version of the class, like this:
		// if ( ! $this->example instanceof CustomExample ) {
		// 	$this->example = CustomExample::init();
		// }
		//
		// return $this->example;
	}

	/**
	 * @return FrameworkWCRender
	 */
	public function wc(): FrameworkWCRender
	{
		return parent::wc();
	}

	/**
	 * @return FrameworkACFRender
	 */
	public function acf(): FrameworkACFRender
	{
		return parent::acf();
	}

	/**
	 * @return FrameworkThemeRender
	 */
	public function render(): FrameworkThemeRender
	{
		return parent::render();
	}

	/**
	 * @return FrameworkThemeImages
	 */
	public function images(): FrameworkThemeImages
	{
		return parent::images();
	}

	/**
	 * @return FrameworkThemeStyleguide
	 */
	public function styleguide(): FrameworkThemeStyleguide
	{
		return parent::styleguide();
	}

	/**
	 * @return FrameworkConfig
	 */
	public function config(): FrameworkConfig
	{
		return parent::config();
	}

	/**
	 * Calls wp_die() with a message about missing a required plugin
	 *
	 * @param string $pluginName
	 */
	private function wpDieMissingPlugin( string $pluginName ): void
	{
		wp_die( $pluginName . ' plugin is missing. Please check if it is installed and activated.' );
	}
}
