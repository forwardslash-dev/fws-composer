<?php
declare( strict_types=1 );

namespace FWS\Framework;

/**
 * Class Composer
 *
 * @package FWS\Framework
 * @author Nikola Topalovic <nick@forwardslashny.com>
 */
class Composer
{
	public static function postInstall()
	{
		$dir = dirname(__FILE__);
		$theme_root = getcwd();

		// FILE: /.fwsconfig.yml
		if ( ! file_exists( $theme_root . '/.fwsconfig.yml' ) ) {
			copy( $dir . '/../resources/' . '.fwsconfig.yml', $theme_root . '/.fwsconfig.yml' );
		}

		// DIR: /fws
		if ( ! file_exists( $theme_root . '/fws' ) ) {
			mkdir( $theme_root . '/fws', 0775, true );
		}

		// FILE: /fws/FWS.php
		if ( ! file_exists( $theme_root . '/fws/FWS.php' ) ) {
			copy( $dir . '/../resources/fws/FWS.php.res', $theme_root . '/fws/FWS.php' );
		}

		// DIR: /fws/Example
		if ( ! file_exists( $theme_root . '/fws/Example' ) ) {
			mkdir( $theme_root . '/fws/Example', 0775, true );
		}

		// FILE: /fws/Example/Example.php
		if ( ! file_exists( $theme_root . '/fws/Example/Example.php' ) ) {
			copy( $dir . '/../resources/fws/Example/Example.php.res', $theme_root . '/fws/Example/Example.php' );
		}

		// FILE: /fws/Example/ExampleHooks.php
		if ( ! file_exists( $theme_root . '/fws/Example/ExampleHooks.php' ) ) {
			copy( $dir . '/../resources/fws/Example/ExampleHooks.php.res', $theme_root . '/fws/Example/ExampleHooks.php' );
		}
	}
}
