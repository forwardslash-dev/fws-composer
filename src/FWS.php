<?php
declare( strict_types=1 );

namespace FWS\Framework;

use FWS\Framework\ACF\Hooks as ACFHooks;
use FWS\Framework\ACF\Render as ACFRender;
use FWS\Framework\Config\Config;
use FWS\Framework\Example\Example as Example;
use FWS\Framework\Theme\Hooks as ThemeHooks;
use FWS\Framework\Theme\Images as ThemeImages;
use FWS\Framework\Theme\Render as ThemeRender;
use FWS\Framework\Theme\Styleguide as ThemeStyleguide;
use FWS\Framework\WC\Hooks as WCHooks;
use FWS\Framework\WC\Render as WCRender;

/**
 * Singleton Class FWS
 *
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
class FWS extends Singleton
{

	/** @var FWS */
	protected static $instance;

	/** @var WCRender */
	protected $wc;

	/** @var ACFRender */
	protected $acf;

	/** @var ThemeRender */
	protected $render;

	/** @var ThemeImages */
	protected $images;

	/** @var ThemeStyleguide */
	protected $styleguide;

	/** @var Config */
	protected $config;

	/** @var Example */
	protected $example;

	/**
	 * FWS constructor.
	 */
	protected function __construct()
	{
		parent::__construct();

		$this->hooks();
	}

	/**
	 * Hooks Classes initialization
	 */
	protected function hooks(): void
	{
		ThemeHooks::init();

		if ( function_exists( 'acf_add_options_sub_page' ) ) {
			ACFHooks::init();
		}

		if ( function_exists( 'WC' ) ) {
			WCHooks::init();
		}
	}

	/**
	 * @return Example
	 */
	public function example(): Example
	{
		if ( ! $this->example instanceof Example ) {
			$this->example = Example::init();
		}

		return $this->example;
	}

	/**
	 * @return WCRender
	 */
	public function wc(): WCRender
	{
		if ( function_exists( 'WC' ) ) {
			$this->wpDieMissingPlugin( 'WooCommerce' );
		}

		if ( ! $this->wc instanceof WCRender ) {
			$this->wc = WCRender::init();
		}

		return $this->wc;
	}

	/**
	 * @return ACFRender
	 */
	public function acf(): ACFRender
	{
		if ( ! function_exists( 'acf_add_options_sub_page' ) ) {
			$this->wpDieMissingPlugin( 'ACF Pro' );
		}

		if ( ! $this->acf instanceof ACFRender ) {
			$this->acf = ACFRender::init();
		}

		return $this->acf;
	}

	/**
	 * @return ThemeRender
	 */
	public function render(): ThemeRender
	{
		if ( ! $this->render instanceof ThemeRender ) {
			$this->render = ThemeRender::init();
		}

		return $this->render;
	}

	/**
	 * @return ThemeImages
	 */
	public function images(): ThemeImages
	{
		if ( ! $this->images instanceof ThemeImages ) {
			$this->images = ThemeImages::init();
		}

		return $this->images;
	}

	/**
	 * @return ThemeStyleguide
	 */
	public function styleguide(): ThemeStyleguide
	{
		if ( ! $this->styleguide instanceof ThemeStyleguide ) {
			$this->styleguide = ThemeStyleguide::init();
		}

		return $this->styleguide;
	}

	/**
	 * @return Config
	 */
	public function config(): Config
	{
		if ( ! $this->config instanceof Config ) {
			$this->config = Config::init();
		}

		return $this->config;
	}

	/**
	 * Calls wp_die() with a message about missing a required plugin
	 *
	 * @param string $pluginName
	 */
	private function wpDieMissingPlugin( string $pluginName ): void
	{
		wp_die( $pluginName . ' plugin is missing. Please check if it is installed and activated.' );
	}
}
