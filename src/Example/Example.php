<?php
declare( strict_types = 1 );

namespace FWS\Framework\Example;

use FWS\Framework\Singleton;

/**
 * Class Example
 *
 * @package FWS\Framework\Example
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
class Example extends Singleton
{

	/** @var self */
	protected static $instance;

	/**
	 * Outputs an example string
	 */
	public function exampleMethod(): void
	{
		echo 'Outputted from a parent class.<br>';
	}
}
