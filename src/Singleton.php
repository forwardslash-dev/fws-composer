<?php
declare( strict_types = 1 );

namespace FWS\Framework;

/**
 * Class Singleton
 *
 * @package FWS\Framework
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
abstract class Singleton
{

    protected function __construct() {}

    /**
	 * @return static
	 */
	public static function init(): self
	{
		if ( static::$instance === null ) {
			static::$instance = new static();
		}

		return static::$instance;
	}
}
