<?php
declare( strict_types = 1 );

namespace FWS\Framework;

/**
 * Class Singleton
 *
 * @package FWS\Framework
 * @author Boris Djemrovski <boris@forwardslashny.com>
 */
abstract class SingletonHook
{

	/**
	 * Singleton constructor.
	 */
	protected function __construct()
	{
		$this->hooks();
	}

	/**
	 * Initializes itself if not already
	 */
	public static function init(): void
	{
		if ( static::$instance === null ) {
			static::$instance = new static();
		}
	}
}
